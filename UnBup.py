#! /usr/bin/env python
# -*-coding: Utf-8 -* 

import sys
import os

print """\n#############################
#                           #
#         UnBup.py          #
#                           #
############################# By Ari\n"""


def usage() :
	print """\nUnBup.py is a scrypt for UnBup McAfee Quarantine files.\n
		Usage:  python UnBup.py [option] [file.bup] [output_file]

			-d = details file only (no executable)
			-h = help menu"""


######################### Function XorLoop ################################
# Loop through files to perform bitwise xor with key write binary to file #
###########################################################################
def xorLoop(input_file, output_file) :
	try :
		xor_bup_file = bytearray(open(input_file, "rb").read())
		for i in range(len(xor_bup_file)) :
			xor_bup_file[i] ^= 0x6a
			#print xor_bup_file[i]
		unbup_file = open(output_file, "wb").write(xor_bup_file)
		#xor_bup_file.close()
		#unbup_file.close()
	except :
		print "Bad [file].bup"
		

################ Function createDetails #################
# Create the Details.txt file with metadata on bup file #
#########################################################
def createDetails(input_file) :
	print "Extracting encoded files from .bup\n"
	try :
		os.system("7z e %s" % input_file)
		print "Creating the Details.txt file\n"
		xorLoop("Details", "Details.txt")
	except :
		print "Bad [file].bup"


########### Function ExtractBinary ###############
# Extracts the original binary from the bup file #
##################################################
def extractBinary() :
	field = os.popen("grep OriginalName Details.txt | awk -F '\\' '{ print NF }'")
	field = field.read()
	#print "Field = ", str(field)
	name = os.popen("grep OriginalName Details.txt | cut -d '\\' -f %s" % field)
	Original_Name = name.read()
	print "Original Name = ", Original_Name
	try :
		Final_Name = sys.argv[2]
	except :
		Final_Name = Original_Name
	xorLoop(input_file, Final_Name)


if len(sys.argv) < 2 :
	usage()
	exit()
elif sys.argv[1] == "-h" :
	usage()
elif sys.argv[1] == "-d" :
	try :
		createDetails(sys.argv[2])
		os.remove('Details')
		os.remove('File_0')
	except :
		print "Bad [file].bup"
else :
	try :
		input_file = sys.argv[1]
		createDetails(input_file)
		extractBinary()
		os.remove('Details')
		os.remove('File_0')
	except :
		print "Fuck off"
